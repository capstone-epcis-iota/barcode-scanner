package com.scanner;

import android.util.Log;

public class AIParser {

    public String code;

    public AIParser(){
        code = "";
    }

    public Boolean parseScan(String results){

        //Remove leading white space;
        //code = results.substring(1);

        code = results;
        Log.d("parsing", code);

        switch(code.substring(0, 2)){

            case("01"):
                Log.d("parsing", code);

                Log.d("parsing", "Prefix: "+
                        code.substring(3,10)+"\n"+
                        "id: "+code.substring(10,15)+code.charAt(2));

                if(code.length() <= 16){
                    return false;
                } else if (code.startsWith("21", 16)){
                    code = "urn:epc:id:sgtin:"
                            +code.substring(3,10)+"."
                            +code.substring(10,15)+code.charAt(2)
                            +"."+code.substring(18);
                    return true;

                }
                return false;

            case ("80"):
                if (code.startsWith("8003")) {
                    Log.d("parsing", "GRAI");
                    if (code.length() <= 18) {
                        return false;
                    } else {

                        /*
                        Note: GRAI is a 14 digit numerical number, including a zero in the leftmost
                        position and the ending Check Digit. The combined length of the GS1 Company
                        Prefix and Asset Type will always total 12 digits.
                        */
                        Log.d("parsing", "Prefix: "+
                                code.substring(5,12)+"\n"+
                                "id: "+code.substring(12,17)+code.charAt(2));

                        code = "urn:epc:id:grai:"
                                +code.substring(5,12)+"."
                                +code.charAt(4)
                                +code.substring(12,17)
                                +"."+code.substring(20);

                        return true;
                    }
                } else if(code.startsWith("8004")) {
                    Log.d("parsing", "GIAI");
                    if(code.length() <= 18) {
                        return false;
                    } else {

                        //Todo add GRAI parsing
                    /*
                    Variable length
                     */
                        code = "urn:epc:id:giai:"
                                +code.substring(5);
                    }
                    return true;
                }

            case("00"):
                Log.d("parsing", "SSCC");
                //Check digit precedes prefix at 2nd index
                char check = results.charAt(2);

                //Company prefix is 8 digits starting from 3rd index
                String pref = results.substring(3,10);

                //Serialization is all remaining characters.
                String serial = results.substring(10);

                code = "urn:epc:id:sscc:"
                        +pref+"."
                        +check+"."
                        +serial;
                return true;
        }

        return false;
    }

}
