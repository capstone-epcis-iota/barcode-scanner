package com.scanner;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

public class Alert {
    // Alert with Message and ok
    public static void showAlertDialog(Context context, String title , String msg){

        Dialog dialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setIcon(android.R.drawable.ic_dialog_info)
                .create();
        dialog.show();
    }

    // Alert with Message and ok Click Event
    public static void showAlertDialog(Context context, String title ,String msg,
                                       DialogInterface.OnClickListener listener){

        Dialog dialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton("OK", listener)
                .setNegativeButton("Cancel", listener)
                .create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public static void showInfoDialog(Context context, String title ,String msg,
                                       DialogInterface.OnClickListener listener){

        Dialog dialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton("OK", listener)
                .create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}