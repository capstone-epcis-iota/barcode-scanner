package com.scanner;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.scanner.cbv.BizStepType;
import com.scanner.event.EPCISEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CaptureUtil {

    public JSONObject putEventId(JSONObject base, String eventId) throws JSONException {
        base.put("eventID", eventId);
        return base;
    }

    public JSONObject putEventTime(JSONObject base, String eventTime)
            throws JSONException {
        base.put("eventTime", eventTime);
        return base;
    }

    public JSONObject putEventTimeZoneOffset(JSONObject base, String eventTimeZoneOffset)
            throws JSONException {
        base.put("eventTimeZoneOffset", eventTimeZoneOffset);
        return base;
    }

    //DOCUMENT SETTINGS
    public JSONObject putContext(JSONObject base, String context)
            throws JSONException {
        base.put("@context", context);
        return base;
    }

    public JSONObject putCreationDate(JSONObject base, String creationDate)
            throws JSONException {
        base.put("creationDate", creationDate);
        return base;
    }

    public JSONObject putSchemaVersion(JSONObject base, String schemaVersion)
            throws JSONException {
        base.put("schemaVersion", schemaVersion);
        return base;
    }

    public JSONObject putFormat(JSONObject base, String format)
            throws JSONException {
        base.put("format", format);
        return base;
    }

    public JSONObject putEPCISBody(JSONObject base, List<EPCISEvent> epcisBody)
            throws JSONException {

        JSONArray jArray = new JSONArray();
        for (EPCISEvent j:epcisBody){
            jArray.put(j.jsonObject());
        }

        base.put("eventList", jArray);
        return base;
    }

    //EVENT SETTINGS
    public JSONObject putEventType(JSONObject base, String eventType)
            throws JSONException {
        base.put("type", eventType);
        return base;
    }

    public JSONObject putAction(JSONObject base, String action)
            throws JSONException {
        base.put("action", action);
        return base;
    }

    public JSONObject putChecksum (JSONObject base, String checksum)
            throws JSONException {
        base.put("checksum", checksum);
        return base;
    }

    public JSONObject putParentID(JSONObject base, String parentID)
            throws JSONException {
        base.put("parentID", parentID);
        return base;
    }

    public JSONObject putEPCList(JSONObject base, List<String> epcList)
            throws JSONException {

        JSONArray array = new JSONArray();
        for (int i = 0; i < epcList.size(); ++i){
            array.put(epcList.get(i));
        }
        base.put("epcList", array);
        return base;
    }

    public JSONObject putChildEPCs(JSONObject base, List<String> epcList)
            throws JSONException {
        JSONArray array = new JSONArray();
        for (int i = 0; i < epcList.size(); ++i){
            array.put(epcList.get(i));
        }
        base.put("childEPCs", array);
        return base;
    }

    public JSONObject putInputEPCList(JSONObject base, List<String> epcList)
        throws JSONException {
        JSONArray array = new JSONArray();
        for (int i = 0; i < epcList.size(); ++i){
            array.put(epcList.get(i));
        }
        base.put("inputEPCList", array);
        return base;
    }

    public JSONObject putOutputEPCList(JSONObject base, List<String> epcList)
            throws JSONException {
        JSONArray array = new JSONArray();
        for (int i = 0; i < epcList.size(); ++i){
            array.put(epcList.get(i));
        }
        base.put("outputEPCList", array);
        return base;
    }

    public JSONObject putTransformationID(JSONObject base, String transformationID)
            throws JSONException {
        base.put("transformationID", transformationID);
        return base;
    }

    public JSONObject putBizStep(JSONObject base, BizStepType bizStep)
            throws JSONException {
        base.put("bizStep", bizStep.getType());
        return base;
    }

    public JSONObject putDisposition(JSONObject base, String disposition) throws JSONException {
        base.put("disposition", disposition);
        return base;
    }

    public JSONObject putReadPoint(JSONObject base, String readPoint) throws JSONException {
        JSONObject rp = new JSONObject();
        rp.put("id", readPoint);
        base.put("readPoint", rp);
        return base;
    }

    public JSONObject putBizLocation(JSONObject base, String bizLocation) throws JSONException {
        JSONObject bl = new JSONObject();
        bl.put("id", bizLocation);
        base.put("bizLocation", bl);
        return base;
    }

    public JSONObject putBizTransactionList(JSONObject base, List<HashMap<String,String>> transactionList)
            throws JSONException {

        JSONArray array = new JSONArray();
        for(HashMap<String, String> data : transactionList) {
            JSONObject obj = new JSONObject(data);
            array.put(obj);
        }

        base.put("bizTransactionList", array);
        return base;
    }

    public JSONObject putInputQuantityList(JSONObject transformationEvent, List<Integer> inputQuantityList) {
        return null;
    }

    public JSONObject putOutputQuantityList(JSONObject transformationEvent, List<Integer> outputQuantityList) {
        return null;
    }
}
