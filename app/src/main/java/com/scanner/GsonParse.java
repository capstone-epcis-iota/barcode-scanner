package com.scanner;

public class GsonParse {

    private String Event;
    private String Action;
    private String Bizstep;
    private String Disposition;
    private String BizLocation;
    private String Readpoint;

    public String getEventType() {
        return Event;
    }

    public void setEventType(String eventType) {
        this.Event = eventType;
    }

    public String getAction() {
        return Action;
    }

    public void setAction(String action) {
        this.Action = action;
    }

    public String getBizstep() {
        return Bizstep;
    }

    public void setBizstep(String bizstep) {
        this.Bizstep = bizstep;
    }

    public String getDisposition() {
        return Disposition;
    }

    public void setDisposition(String disposition) {
        this.Disposition = disposition;
    }

    public String getBizLocation() {
        return BizLocation;
    }

    public void setBizLocation(String bizLocation) {
        this.BizLocation = bizLocation;
    }

    public String getReadpoint() {
        return Readpoint;
    }

    public void setReadpoint(String readpoint) {
        this.Readpoint = readpoint;
    }
}
