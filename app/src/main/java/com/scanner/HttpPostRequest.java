package com.scanner;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class HttpPostRequest {

    String url = "http://ec2-52-37-5-22.us-west-2.compute.amazonaws.com:4000/capture";
    String url2 = null;

    public void postActivity(Context context, JSONObject epcisObj) {

        //BEGIN POST
        Toast.makeText(context,
                "Posting event.", Toast.LENGTH_LONG ).show();
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                url,
                epcisObj, (Response.Listener<JSONObject>) response -> {
            Log.d("RESPONSE:", String.valueOf(response));
            Toast.makeText(context,
                    "Successfully posted event.", Toast.LENGTH_LONG ).show();
        }, error -> {
            Toast.makeText(context,
                    "Error posting event.\n"+
                    error.toString(), Toast.LENGTH_LONG ).show();
        });

        jsonRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) {

            }
        });

        Volley.newRequestQueue(context).add(jsonRequest);
    }

    public String parseVolleyError(VolleyError error) {
        try {
            String responseBody = new String(error.networkResponse.data, "utf-8");
            JSONObject data = new JSONObject(responseBody);
            JSONArray errors = data.getJSONArray("errors");
            JSONObject jsonMessage = errors.getJSONObject(0);
            return jsonMessage.getString("message");
        } catch (JSONException | UnsupportedEncodingException e) {
            error.printStackTrace();
        }
        return null;
    }

}
