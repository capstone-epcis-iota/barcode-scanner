package com.scanner.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.scanner.AIParser;
import com.scanner.Alert;
import com.scanner.HttpPostRequest;
import com.scanner.R;
import com.scanner.cbv.BizStepType;
import com.scanner.cbv.BizTransactionType;
import com.scanner.event.AggregationEvent;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AggregationActivity extends AppCompatActivity implements View.OnClickListener {

    //String for logging activity.
    String TAG = "AGGREGATIONACTIVITY";

    //Scan result contents + barcode format.
    String scanResults;
    String format;

    //Event variables
    JSONObject epcisObj;
    AggregationEvent aggEvent;
    ArrayList<String> epcList = new ArrayList<>();
    ArrayList<Integer> quantityList = new ArrayList<>();
    String parentId;

    AIParser parser = new AIParser();

    //Values for capturing business transactions.
    ArrayList<String> bttType = new ArrayList<>();
    ArrayList<String> bttValue = new ArrayList<>();
    HashMap<String, String> bttChecksum = new HashMap<>();
    int k = 0;
    String checkSum;

    //Variable for aggregation vs disaggregation control
    String msg = "";
    String title = "";
    String errMsg = "";

    //TextViews
    TextView parentEPC;
    ListView childEPClist;
    ArrayAdapter<String> adapter;

    //Buttons
    Button btnScan;
    Button btnClear;
    Button btnAdd;
    Button btnOther;
    Button btnRem;
    Button btnSubmit;
    Button btnExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Set title of activity
        setTitle(getIntent().getStringExtra("TITLE"));
        setContentView(R.layout.activity_aggregation_scan);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);

        //TextView Setup
        parentEPC = findViewById(R.id.parentEPC);
        childEPClist = findViewById(R.id.childEPClist);

        //Button setup
        btnScan = findViewById(R.id.btn_scan);
        btnScan.setOnClickListener(this);
        btnClear = findViewById(R.id.btn_clear);
        btnClear.setOnClickListener(this);
        btnAdd = findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);
        btnOther = findViewById(R.id.btn_other);
        btnOther.setOnClickListener(this);
        btnRem = findViewById(R.id.btn_remove);
        btnRem.setOnClickListener(this);
        btnSubmit = findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(this);
        btnExit = findViewById(R.id.btn_exit);
        btnExit.setOnClickListener(this);

        //Provide contextual messages to the user based on activity.
        switch (getIntent().getStringExtra("ACTION")) {
            case "ADD":
                title = "Scanner Configured for Aggregation!";
                msg = "This mode allows for associating individual products with a shipping " +
                        "container or pallet.\n\nTo begin, scan the SSCC of the pallet/container " +
                        "and then any number of product EPCs to be associated with that label.\n\n" +
                        "Press SUBMIT to post the event to the database.";
                break;
            case "DELETE":
                title = "Scanner Configured for Dis-Aggregation!";
                msg = "This mode allows for removing one, or many, individual products from a " +
                        "shipping container or pallet.\n\nTo begin, scan the SSCC of the " +
                        "pallet/container and then any number of product EPCs to be disassociated " +
                        "from that label.\n\nPress SUBMIT to post the event to the database.";
                break;
            case "OBSERVE":
                title = "Scanner Configured for Observation!";
                msg = "This mode allows for tracking/observation of a pallet/shipping container " +
                        "and its associated products.\n\nTo begin, scan the SSCC of " +
                        "pallet/container and ALL product EPCs associated with that label." +
                        "\n\nPress SUBMIT to post the event to the database.";
                break;
            default:
                finish();
                break;
        }

        Alert.showAlertDialog(this,title,msg, (dialog, which) -> {
            if (!(which == DialogInterface.BUTTON_POSITIVE))
                finish();
        });
    }


    @Override
    public void onClick(View v) {
        //Buttons for scenarios
        if (v == btnSubmit){

            //Build error message.
            int error = 0;
            if (epcList.size() == 0){ errMsg += "EPC List is empty.\n"; error = 1;}
            if (parser.code.equals("")) { errMsg += "SSCC is empty."; error = 1; }

            if (error == 1) {
                Alert.showInfoDialog(this, "Error!",
                        "Missing required field(s).\n\n" + errMsg, (dialog, which) -> errMsg = "");
            } else {
                //Build the EPCIS event and attempt to post.
                try {
                    buildEvent();
                    postActivity();
                } catch (JSONException | NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }
        } else if (v == btnScan) {
            Log.d(TAG, "Scan button clicked.");

            //Initiate scan of parent SSCC
            initParentScan();

        } else if (v == btnClear) {
            Log.d(TAG, "Clear button clicked.");

            if(parser.code != null)
                //Reset/clear the SSCC field.
                clear();
            else
                //If the EPClist is empty, alert the user.
                Alert.showInfoDialog(this,"Error!",
                        "SSCC is already cleared", (dialog, which) -> { });

        } else if (v == btnOther) {
            Log.d(TAG, "Other button clicked.");
            bttDisplay();
        } else if (v == btnRem) {
            //If the EPClist is not empty, remove the most recent addition.
            if(epcList.size() > 0){
                restart();
            } else
                //If the EPClist is empty, alert the user.
                Alert.showInfoDialog(this,"Error!",
                        "EPC list is empty.", (dialog, which) -> { });

        } else if (v == btnAdd) {
            Log.d(TAG, "Add button clicked.");
            initChildScan();

        } else if (v == btnExit){

            //Exit program
            onBackPressed();
        }
    }


    public void initParentScan(){
        Intent intent = new Intent(AggregationActivity.this, ScanActivity.class);
        parentSSCCcapture.launch(intent);
    }

    ActivityResultLauncher<Intent> parentSSCCcapture = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), activityResult -> {
                int result = activityResult.getResultCode();
                Intent data = activityResult.getData();

                if (result == RESULT_OK) {

                    //Do something
                    if (data != null) {

                        format = data.getStringExtra("format");
                        scanResults = data.getStringExtra("content");

                        Log.d("EPC", format + "\n" + scanResults);

                        if (parser.parseScan(scanResults) && format.equals("CODE_128")) {
                            Dialog dialog = new AlertDialog.Builder(this)
                                    .setIcon(android.R.drawable.ic_dialog_info)
                                    .setTitle("Scan Result")
                                    .setMessage(parser.code)
                                    .setNeutralButton("Rescan", (dialog2, which) -> {
                                        //Remove SSCC and rescan.
                                        parser.code = "";
                                        initParentScan();
                                    })
                                    .setPositiveButton("Accept", (dialog2, which) -> {
                                        //Set parentEPC field text to accepted SSCC
                                        parentId = parser.code;
                                        parentEPC.setText(parser.code);
                                    })
                                    .show();
                            dialog.setCanceledOnTouchOutside(false);

                        } else {
                            Dialog dialog = new AlertDialog.Builder(this)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Error")
                                    .setMessage("Invalid SSCC Label")
                                    .setPositiveButton("Ok", null)
                                    .show();
                            dialog.setCanceledOnTouchOutside(false);
                        }
                    } else {
                        //Don't do something
                        Toast.makeText(AggregationActivity.this,
                                "Error",
                                Toast.LENGTH_LONG).show();
                    }
                }
            }
    );

    public void initChildScan(){
        Intent intent = new Intent(AggregationActivity.this, ScanActivity.class);
        childEPCcapture.launch(intent);
    }

    ActivityResultLauncher<Intent> childEPCcapture = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), activityResult -> {
                int result = activityResult.getResultCode();
                Intent data = activityResult.getData();

                if (result == RESULT_OK) {

                    //Do something
                    if (data != null) {

                        format = data.getStringExtra("format");
                        scanResults = data.getStringExtra("content");

                        Log.d("EPC", format+"\n"+scanResults);

                        //Parse the EPC and check its format
                        if(parser.parseScan(scanResults) && (format.equals("DATA_MATRIX")
                                || format.equals("CODE-128"))){
                            if(epcList.contains(parser.code)){
                                //Check if the epc already has been scanned
                                Dialog dialog = new AlertDialog.Builder(this)
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setTitle("Error")
                                        .setMessage("EPC has already been scanned.")
                                        .setPositiveButton("Ok", (dialog2, which) -> {
                                            adapter.clear();
                                            for (int i = 0; i < epcList.size(); ++i)
                                                adapter.add(epcList.get(i));
                                            childEPClist.setAdapter(adapter);
                                        })
                                        .show();
                                dialog.setCanceledOnTouchOutside(false);
                            } else {

                                epcList.add(parser.code);
                                //Default quantity is 1;
                                quantityList.add(1);

                                Dialog dialog = new AlertDialog.Builder(this)
                                        .setIcon(android.R.drawable.ic_dialog_info)
                                        .setTitle("Scan Result")
                                        .setMessage(parser.code)
                                        .setPositiveButton("Next", (dialog2, which) -> {
                                            //Scan next product
                                            initChildScan();
                                        })
                                        .setNegativeButton("Rescan", (dialog2, which) -> {
                                            //Remove epc
                                            epcList.remove(epcList.size() - 1);
                                            Toast.makeText(getApplicationContext(),
                                                    "Rescanning...", Toast.LENGTH_SHORT)
                                                    .show();
                                            //Rescan
                                            initChildScan();
                                        })
                                        .setNeutralButton("Finish", (dialog2, which) -> {
                                            adapter.clear();
                                            for (int i = 0; i < epcList.size(); ++i){
                                                adapter.add(epcList.get(i));
                                            }
                                            childEPClist.setAdapter(adapter);
                                        })
                                        .show();
                                dialog.setCanceledOnTouchOutside(false);
                                dialog.setOnCancelListener(dialog1 ->
                                        epcList.remove(epcList.size() - 1));
                            }
                        } else {
                            //Currently unsupported format.
                            Dialog dialog = new AlertDialog.Builder(this)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Error")
                                    .setMessage("This barcode format is not supported.")
                                    .setPositiveButton("Ok", (dialog2, which) -> {
                                        adapter.clear();
                                        for (int i = 0; i < epcList.size(); ++i)
                                            adapter.add(epcList.get(i));
                                        childEPClist.setAdapter(adapter);
                                    })
                                    .show();
                            dialog.setCanceledOnTouchOutside(false);
                        }

                    } else {
                        Toast.makeText(AggregationActivity.this, "Error",
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    //Don't do something
                    Toast.makeText(AggregationActivity.this,
                            "Error",
                            Toast.LENGTH_LONG).show();
                }
            }
    );

    public void bttDisplay(){

        //TRANSACTION PRINTER
        final int[] checkedItem = {-1};
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Choose an additional item to add to this event:");

        /* Generate list from BTT types */
        String[] keys = new BizTransactionType()
                .getTypes()
                .keySet()
                .toArray(new String[0]);
        String[] listItems = new BizTransactionType()
                .getTypes()
                .values()
                .toArray(new String[0]);

        alertDialog.setSingleChoiceItems(listItems, checkedItem[0], (dialog, which) -> {

            checkedItem[0] = which;

            bttType.add(k, keys[which]);

            Alert.showAlertDialog(this,"Adding "+listItems[which],
                    "Scan QR to add business transaction reference to this event.",
                    (dialog2, which2) -> {
                        if ((which2 == DialogInterface.BUTTON_POSITIVE))
                            initBTTScan();
                    });
            dialog.dismiss();
        });

        alertDialog.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
        AlertDialog customAlertDialog = alertDialog.create();
        customAlertDialog.show();
    }

    public void initBTTScan(){
        Intent intent = new Intent(AggregationActivity.this, ScanActivity.class);
        BTTcapture.launch(intent);
    }

    ActivityResultLauncher<Intent> BTTcapture = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), activityResult -> {
                int result = activityResult.getResultCode();
                Intent data = activityResult.getData();

                if (result == RESULT_OK) {

                    //Do something
                    if (data != null) {

                        format = data.getStringExtra("format");
                        scanResults = data.getStringExtra("content");

                        //Check if the scan is of QR_CODE format.
                        if (!format.equals("QR_CODE")) {
                            //Currently unsupported format.
                            String error = "Currently only QR codes are supported.";
                            Dialog dialog = new AlertDialog.Builder(this)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Error")
                                    .setMessage(error)
                                    .setPositiveButton("Ok", (dialog2, which) -> initBTTScan())
                                    .show();
                            dialog.setCanceledOnTouchOutside(false);
                        } else {

                            //PARSE SCAN RESULTS FOR URL
                            if (URLUtil.isValidUrl(scanResults) && scanResults.endsWith(".pdf")) {
                                Dialog dialog;
                                dialog = new AlertDialog.Builder(this)
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setTitle("Document link detected!")
                                        .setMessage(scanResults + "\nThis is a link to a document." +
                                                "\nDo you want to create a validation check sum?")
                                        .setPositiveButton("Yes", (dialog2, which) ->{
                                            validateDoc(scanResults);
                                            bttValue.add(k, scanResults);
                                            checkSum = null;
                                            adapter.add(scanResults);
                                            ++k;
                                        })
                                        .setNegativeButton("No", (dialog2, which) -> {

                                        })
                                        .show();
                                dialog.setCanceledOnTouchOutside(false);
                            } else {
                                Dialog dialog3 = new AlertDialog.Builder(this)
                                        .setIcon(android.R.drawable.ic_dialog_info)
                                        .setTitle("Scan Result")
                                        .setMessage("Type: "+format+"\nContent: "+scanResults)
                                        .setNegativeButton("Rescan", (dialog4, which2) -> {
                                            Toast.makeText(getApplicationContext(),
                                                    "Rescanning...",Toast.LENGTH_SHORT)
                                                    .show();
                                            //Rescan
                                            initBTTScan();
                                        })
                                        .setNeutralButton("Finish", (dialog4, which2) -> {
                                            bttValue.add(k, scanResults);
                                            adapter.add(scanResults);
                                            ++k;
                                        })
                                        .show();
                                dialog3.setCanceledOnTouchOutside(false);
                                dialog3.setOnCancelListener(dialog1 ->
                                        epcList.remove(epcList.size() - 1));
                            }
                        }

                    } else {
                        Toast.makeText(AggregationActivity.this, "Error",
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    //Don't do something
                    Toast.makeText(AggregationActivity.this,
                            "Error",
                            Toast.LENGTH_LONG).show();
                }
            }
    );

    public void validateDoc(String url){
        Intent intent = new Intent(AggregationActivity.this, ValidationActivity.class);
        intent.putExtra("url", url);
        docCapture.launch(intent);
    }

    ActivityResultLauncher<Intent> docCapture = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), activityResult -> {
                int result = activityResult.getResultCode();
                Intent data = activityResult.getData();

                if (result == RESULT_OK) {
                    //Do something
                    if (data != null) {
                        checkSum = data.getStringExtra("checksum");
                        bttChecksum.put(scanResults, checkSum);
                    }
                }
            }
    );

    public void buildEvent() throws JSONException, NoSuchAlgorithmException {

        aggEvent = new AggregationEvent();

        //Get config variables from main activity.
        aggEvent.setAction(this.getIntent().getStringExtra("ACTION"));
        aggEvent.setBizStep(new BizStepType(this.getIntent().getStringExtra("BIZSTEP")));
        aggEvent.setDisposition(this.getIntent().getStringExtra("DISPOSITION"));
        aggEvent.setBizLocation(this.getIntent().getStringExtra("BIZLOCATION"));
        aggEvent.setReadPoint(this.getIntent().getStringExtra("READPOINT"));

        //Set event specific variables
        aggEvent.setEventTimeZoneOffset();
        aggEvent.setParentID(parentId);
        aggEvent.setChildEPCs(epcList);
        aggEvent.setChildQuantityList(quantityList);

        if(k > 0) {
            List<HashMap<String,String>> map = new ArrayList<>();
            for (int i = 0; i < k; ++i){
                HashMap<String, String> bttMap = new HashMap<>();
                bttMap.put("type", bttType.get(i));
                bttMap.put("bizTransaction", bttValue.get(i));

                if(bttChecksum.get(bttValue.get(i)) != null){
                    bttMap.put("checksum", bttChecksum.get(bttValue.get(i)));
                }
                map.add(bttMap);
            }
            aggEvent.setBizTransactionList(map);
        }


        //Hash the value of the event to generate a unique ID.
        aggEvent.setEventId();

        try {
            epcisObj = aggEvent.jsonObject();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void postActivity() throws JSONException {

        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle("Posting")
                .setMessage("Submitting scan ativity of ("+quantityList.size()+") scans?")
                .setPositiveButton("Yes", (dialog, which) -> {

                    //HttpPostRequest postRequest = new HttpPostRequest();
                    //postRequest.postActivity(this, epcisObj);

                    //DEBUG PRINTER
                    final TextView input = new TextView (this);
                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
                    JsonParser jp = new JsonParser();
                    JsonElement je = jp.parse(String.valueOf(epcisObj));
                    String prettyJsonString = gson.toJson(je);
                    input.setText(prettyJsonString);
                    input.setTextSize(12);
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setView(input);
                    alert.setPositiveButton("Ok",(dialog2, which2) -> {
                        HttpPostRequest postRequest = new HttpPostRequest();
                        postRequest.postActivity(this, epcisObj);
                        restart();
                    });
                    alert.setNegativeButton("Cancel", null);
                    alert.show();
                    //END DEBUG PRINTER
                })
                .setNegativeButton("No", (dialog, which) -> restart())
                .show();
    }

    public void restart(){
        Log.d(TAG, "Restarting activity");
        reset();
    }

    public void reset(){
        //Call clear activity to remove SSCC
        clear();

        //Reset the epcis/obj events
        quantityList.clear();
        epcList.clear();

        //Rest business transaction events.
        bttType.clear();
        bttValue.clear();
        k = 0;

        //Clear objects
        epcisObj = null;
        aggEvent = null;

        //Reset views
        adapter.clear();
        childEPClist.removeAllViewsInLayout();
    }

    public void clear(){
        parser.code = "";
        parentEPC.setText("");
    }

    @Override
    public void onBackPressed(){
        Alert.showAlertDialog(this,"Exiting!",
                "This will reset the device configuration.\n\n"+
                        "Are you sure you want to proceed?", (dialog, which) -> {
            if ((which == DialogInterface.BUTTON_POSITIVE))
                finish();
        });
    }
}