package com.scanner.activity;

import android.content.Intent;

import android.os.Bundle;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.scanner.GsonParse;
import com.scanner.HttpGetRequest;

import org.json.JSONException;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class ConfigActivity extends AppCompatActivity {

    ActivityResultLauncher<Intent> activityResultLauncher;
    String scanResults;
    String format;

    ActivityResultLauncher<Intent> configCapture = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            activityResult -> {
                int result = activityResult.getResultCode();
                Intent data = activityResult.getData();

                if (result == RESULT_OK) {

                    //Do something
                    if (data != null) {

                        format = data.getStringExtra("format");
                        scanResults = data.getStringExtra("content");

                        if (validateURL(format)){

                            try {
                                extractDataFromURL(scanResults);
                            } catch (IOException
                                    | ExecutionException
                                    | InterruptedException
                                    | JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            //Don't do something
                            Toast.makeText(ConfigActivity.this,
                                    "Error: Invalid configuration format.",
                                    Toast.LENGTH_LONG).show();
                            finish();
                        }
                    } else {
                        //Don't do something
                        Toast.makeText(ConfigActivity.this,
                                "Error: Invalid configuration format.",
                                Toast.LENGTH_LONG).show();
                        finish();
                    }
                } else {
                    //Don't do something
                    Toast.makeText(ConfigActivity.this,
                            "Error: Invalid configuration scanned.",
                            Toast.LENGTH_LONG).show();
                    finish();
                }
            }
    );

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initScan();
    }

    private void initScan(){
        Intent intent = new Intent(ConfigActivity.this, ScanActivity.class);
        configCapture.launch(intent);
    }

    private void extractDataFromURL(String url_string)
            throws IOException, ExecutionException, InterruptedException, JSONException {

        //String to place our result in
        String result;

        //Instantiate new instance of our class
        HttpGetRequest getRequest = new HttpGetRequest();

        try {
            //Perform the doInBackground method, passing in our url
            result = getRequest.execute(url_string).get();

            Gson gson = new Gson();
            GsonParse data = gson.fromJson(result, GsonParse.class);

            getIntent().putExtra("EventType", data.getEventType());
            getIntent().putExtra("Action", data.getAction());
            getIntent().putExtra("BizStep", data.getBizstep());
            getIntent().putExtra("Disposition", data.getDisposition());
            getIntent().putExtra("BizLocation", data.getBizLocation());
            getIntent().putExtra("ReadPoint", data.getReadpoint());

            setResult(RESULT_OK, getIntent());

            finish();
        } catch (NullPointerException e) {
            //Don't do something
            Toast.makeText(ConfigActivity.this,
                    "Error: Invalid configuration scanned.",
                    Toast.LENGTH_LONG).show();
            finish();
        }
    }

    public boolean validateURL(String string){

        return (string.equals("QR_CODE"));
    }

    @Override
    public void onBackPressed(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle("Exiting...")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", (dialog, which) -> finish())
                .setNegativeButton("No", null)
                .show();
    }

}
