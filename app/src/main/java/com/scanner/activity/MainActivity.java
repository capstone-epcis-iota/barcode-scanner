package com.scanner.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.scanner.R;
import com.scanner.cbv.ActionType;
import com.scanner.cbv.DispositionType;
import com.scanner.cbv.Location;
import com.scanner.event.EPCISDocument;
import com.scanner.event.EPCISEvent;
import com.scanner.event.TransactionEvent;

import org.json.JSONException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    String TAG = "Main Activity";
    Intent intent;

    Button btnConfig;
    Button btnExit;

    //Config Variables
    String eventType;
    String action;
    String bizStep;
    String disposition;
    String bizLocation;
    String readPoint;

    ActivityResultLauncher<Intent> configureDevice = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), activityResult -> {
                int result = activityResult.getResultCode();
                Intent data = activityResult.getData();

                if (result == RESULT_OK) {

                    //Do something
                    if (data != null) {

                        eventType = data.getStringExtra("EventType");
                        action = data.getStringExtra("Action");
                        bizStep = data.getStringExtra("BizStep");
                        disposition = data.getStringExtra("Disposition");
                        readPoint = data.getStringExtra("ReadPoint");
                        bizLocation = data.getStringExtra("BizLocation");

                        Log.d("MAIN_data", "\n"+eventType+"\n"+action+"\n"+bizStep+"\n"+disposition);
                        launch();

                    }
                }
            });

    ActivityResultLauncher<Intent> initiateActivity = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), activityResult -> {
                int result = activityResult.getResultCode();
                Intent data = activityResult.getData();

                if (result == RESULT_OK) {

                    //Do something
                    if (data != null) {

                        Toast.makeText(MainActivity.this,
                                "Return to main",
                                Toast.LENGTH_LONG).show();

                        EPCISEvent event = data.getParcelableExtra("event");

                        try {
                            new AlertDialog.Builder(this)
                                    .setIcon(android.R.drawable.ic_dialog_info)
                                    .setTitle("RESULT")
                                    .setMessage(event.jsonObject().toString(3))
                                    .setPositiveButton("Yes", (dialog, which) -> {
                                            //Create epcis document
                                            EPCISDocument epcisDocument = new EPCISDocument(
                                                    "2.0",
                                                    "application/ld+json",
                                                    "https://id.gs1.org/epcis-context.jsonld"
                                            );

                                            epcisDocument.setEpcisBody(event);
                                    })
                                    .setNegativeButton("No", null)
                                    .show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
            });

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Schroedinger Motors EPC Scanner");

        //Assign BUTTON variables
        btnConfig = findViewById(R.id.btn_config);
        btnExit = findViewById(R.id.btn_exit);

        //Set active LISTEN on buttons
        btnConfig.setOnClickListener(this);
        btnExit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        //Buttons for scenarios
        if  (v == btnConfig){
            //Begin configuration scan
            Intent intent = new Intent(MainActivity.this, ConfigActivity.class);
            configureDevice.launch(intent);

        } else if (v == btnExit){
            //Exit program
            onBackPressed();
        }
    }

    public void launch(){

        //Launch activity based on configuration activityType
        switch (eventType){
            case "AggregationEvent":
                Log.d(TAG, "Launching Aggregation Event");
                intent = new Intent(MainActivity.this, AggregationActivity.class);
                //Set title for intent
                intent.putExtra("TITLE", "Aggregation Scanner");
                break;

            case "ObjectEvent":
                intent = new Intent(MainActivity.this, ObjectActivity.class);
                //Set title for intent
                intent.putExtra("TITLE", "Object Scanner");
                break;

            case "TransformationEvent":
                intent = new Intent(MainActivity.this, TransformationActivity.class);
                //Set title for intent
                intent.putExtra("TITLE", "Transformation Scanner");
                break;

            case "TransactionEvent":
                Log.d("Type", "Transaction Event");
                intent = new Intent(MainActivity.this, TransactionActivity.class);
                //Set title for intent
                intent.putExtra("TITLE", "Transaction Scanner");
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + eventType);
        }

        //Set activity variables from configuration
        intent.putExtra("EVENT", eventType);
        //Transformation events don't have actions
        if(!eventType.equals("TransformationEvent")){
            intent.putExtra("ACTION", new ActionType(action).getType());
        }
        intent.putExtra("BIZSTEP", bizStep);
        intent.putExtra("DISPOSITION", new DispositionType(disposition).getType());
        intent.putExtra("READPOINT", new Location(readPoint).getReadpoint());
        intent.putExtra("BIZLOCATION", "urn:epc:id:sgln:"+bizLocation);
        initiateActivity.launch(intent);
    }

    @Override
    public void onBackPressed(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle("Exiting...")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", (dialog, which) -> finish())
                .setNegativeButton("No", null)
                .show();
    }
}
