package com.scanner.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.scanner.Capture;

public class ScanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initScan();
    }

    public void initScan(){

        //Initialize intent integrator (zXing barcode scanner)
        IntentIntegrator intentIntegrator = new IntentIntegrator(
                ScanActivity.this
        );

        //Set prompt text
        intentIntegrator.setPrompt("For flash use volume up key.");

        //Set beep
        intentIntegrator.setBeepEnabled(true);

        //Locked orientation
        intentIntegrator.setOrientationLocked(true);

        //Set capture activity
        intentIntegrator.setCaptureActivity(Capture.class);

        //Initiate scan
        intentIntegrator.initiateScan();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint({"MissingPermission", "HardwareIds"})
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Initialize intent result
        IntentResult intentResult = IntentIntegrator.parseActivityResult(
                requestCode,resultCode,data
        );

        //Check status of scan
        if(intentResult.getContents() != null){

            //When the result is not null, pass scanned info back
            Intent results = new Intent ();
            results.putExtra("content", intentResult.getContents());
            results.putExtra("format", intentResult.getFormatName());
            results.putExtra("class", intentResult.getClass());
            setResult(RESULT_OK, results);
        } else {

            //When result content is null
            //Display toast
            Toast.makeText(getApplicationContext(),
                    "Failed to scan.",Toast.LENGTH_SHORT)
                    .show();
        }

        finish();
    }

    @Override
    public void onBackPressed(){
        finish();
    }

}
