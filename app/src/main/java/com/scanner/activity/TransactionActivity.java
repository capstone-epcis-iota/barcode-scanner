package com.scanner.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.scanner.AIParser;
import com.scanner.Alert;
import com.scanner.HttpPostRequest;
import com.scanner.R;
import com.scanner.cbv.BizStepType;
import com.scanner.cbv.BizTransactionType;
import com.scanner.event.TransactionEvent;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TransactionActivity extends AppCompatActivity implements View.OnClickListener {

    //String for logging activity.
    String TAG = "TRANSACTIONACTIVITY";

    //Scan result contents + barcode format.
    String scanResults;
    String format;

    //Event variables
    JSONObject epcisObj;
    TransactionEvent transactionEvent;
    //List<List<String>> epcList = new ArrayList<>(3);
    ArrayList<String> epcList = new ArrayList<>();
    ArrayList<String> epcList_other = new ArrayList<>();
    ArrayList<Integer> quantityList = new ArrayList<>();

    //Values for capturing business transactions.
    ArrayList<String> bttType = new ArrayList<>();
    ArrayList<String> bttValue = new ArrayList<>();
    HashMap<String, String> bttChecksum = new HashMap<>();
    String checkSum;
    int k = 0;

    //Formatting and displaying text.
    String msg = "";
    String title = "";
    String errMsg = "";

    //TextViews
    ListView EPClist;
    ListView EPClist_other;
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> adapter_other;

    AIParser parser = new AIParser();

    //Buttons
    Button btnAdd;
    Button btnRem;
    Button btnAddOther;
    Button btnRemOther;
    Button btnSubmit;
    Button btnExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(getIntent().getStringExtra("TITLE"));
        setContentView(R.layout.activity_transaction_scan);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        adapter_other = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);

        //TextView Setup
        EPClist = findViewById(R.id.EPClist);
        EPClist_other = findViewById(R.id.otherEPClist);

        //Button setup
        btnAdd = findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);
        btnRem = findViewById(R.id.btn_remove);
        btnRem.setOnClickListener(this);
        btnAddOther = findViewById(R.id.btn_add_other);
        btnAddOther.setOnClickListener(this);
        btnRemOther = findViewById(R.id.btn_remove_other);
        btnRemOther.setOnClickListener(this);
        btnSubmit = findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(this);
        btnExit = findViewById(R.id.btn_exit);
        btnExit.setOnClickListener(this);

        btnSubmit.setEnabled(false);
        btnRem.setEnabled(false);

        title = "Scanner Configured for Transaction Event!";
        msg = "This mode is for adding business transactions to an associated EPC. Scan the EPC of" +
                " the product, and any add any associated business transaction documents or URNs by" +
                " using their relevant GS1 datamatrix.";
        Alert.showAlertDialog(this,title,msg, (dialog, which) -> {
            if (!(which == DialogInterface.BUTTON_POSITIVE))
                finish();
        });
    }

    @Override
    public void onClick(View v) {

        if (btnSubmit.equals(v)) {//Alert the user if there are missing fields prevent submission.
            if (epcList.size() == 0) {
                errMsg += "Input EPC List is empty.\n";
                Alert.showInfoDialog(this, "Error!",
                        "Missing required field(s).\n\n"
                                + errMsg, (dialog, which) -> errMsg = "");
            } else if (k == 0){
                errMsg += "Additional documentation is empty.\n";
                Alert.showInfoDialog(this, "Error!",
                        "Missing required field(s).\n\n"
                                + errMsg, (dialog, which) -> errMsg = "");
            } else {
                //Build the EPCIS event and attempt to post.
                try {
                    buildEvent();
                    postActivity();

                } catch (JSONException | NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }
        } else if (btnExit.equals(v)) {
            onBackPressed();
        } else if (btnAddOther.equals(v)) {//Logic for adding of biz transaction items.
            if (epcList.size() == 0) {
                //If the EPClist is empty, alert the user.
                Alert.showInfoDialog(this, "Error!",
                        "EPC list is empty. Before adding additional documents," +
                                " please scan one or more EPCs for this event.",
                        (dialog, which) -> {
                        });
            } else {
                bttDisplay();
            }

        } else if (btnRemOther.equals(v)) {//Logic for adding of biz transaction items.
            if (epcList.size() == 0) {
                //If the EPClist is empty, alert the user.
                Alert.showInfoDialog(this, "Error!",
                        "Input or Output list is empty",
                        (dialog, which) -> {
                        });
            } else {
                adapter_other.clear();
                epcList_other.clear();
                EPClist_other.removeAllViewsInLayout();
            }
        } else if (btnRem.equals(v)) {
            if (epcList.size() > 0) {
                //Clear EPC list
                epcList.clear();
                adapter.clear();
                EPClist.removeAllViewsInLayout();
            } else
                //If the EPClist is empty, alert the user.
                Alert.showInfoDialog(this, "Error!",
                        "EPC list is empty.", (dialog, which) -> {
                        });
        } else if (btnAdd.equals(v)) {
            initEPCScan();
        }
    }

    public void initEPCScan(){
        Intent intent = new Intent(TransactionActivity.this, ScanActivity.class);
        EPCcapture.launch(intent);
    }


    ActivityResultLauncher<Intent> EPCcapture = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), activityResult -> {
                int result = activityResult.getResultCode();
                Intent data = activityResult.getData();

                if (result == RESULT_OK) {

                    //Do something
                    if (data != null) {

                        format = data.getStringExtra("format");
                        scanResults = data.getStringExtra("content");
                        Log.d("EPC", format+"\n"+scanResults);

                        // CHECK WHAT TYPE OF PRODUCT IS OBSERVED
                        if(parser.parseScan(scanResults) && (format.equals("DATA_MATRIX"))) {

                            Dialog dialog;
                            if(epcList.contains(parser.code)){

                                //Check if the epc already has been scanned
                                dialog = new AlertDialog.Builder(this)
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setTitle("Error")
                                        .setMessage("EPC has already been scanned.")
                                        .setPositiveButton("Ok", (dialog2, which) -> {
                                                adapter.clear();
                                                for (int i = 0; i < epcList.size(); ++i) {
                                                    adapter.add(epcList.get(i));
                                                    quantityList.add(1);
                                                }
                                                EPClist.setAdapter(adapter);
                                            })
                                        .show();
                            } else {

                                dialog = new AlertDialog.Builder(this)
                                        .setIcon(android.R.drawable.ic_dialog_info)
                                        .setTitle("Scan Result")
                                        .setMessage(parser.code)
                                        .setPositiveButton("Next", (dialog2, which) -> {

                                            epcList.add(parser.code);

                                            //Scan next product
                                            initEPCScan();
                                        })
                                        .setNegativeButton("Rescan", (dialog2, which) -> {
                                            //Remove epc
                                            Toast.makeText(getApplicationContext(),
                                                    "Rescanning...", Toast.LENGTH_SHORT)
                                                    .show();
                                            //Rescan
                                            initEPCScan();
                                        })
                                        .setNeutralButton("Finish", (dialog2, which) -> {

                                            adapter.clear();
                                            epcList.add(parser.code);

                                            for (int i = 0; i < epcList.size(); ++i) {
                                                adapter.add(epcList.get(i));
                                                quantityList.add(1);
                                            }
                                            EPClist.setAdapter(adapter);

                                            btnSubmit.setEnabled(true);
                                            btnRem.setEnabled(true);
                                        })
                                        .show();

                            }
                            dialog.setCanceledOnTouchOutside(false);
                        } else {
                            //Currently unsupported format.
                            Dialog dialog = new AlertDialog.Builder(this)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Error")
                                    .setMessage("This barcode format is not supported.")
                                    .setPositiveButton("Ok", (dialog2, which) -> {
                                        adapter.clear();
                                        for (int i = 0; i < epcList.size(); ++i)
                                            adapter.add(epcList.get(i));
                                        EPClist.setAdapter(adapter);
                                    })
                                    .show();
                            dialog.setCanceledOnTouchOutside(false);
                        }

                    } else {
                        Toast.makeText(TransactionActivity.this, "Error",
                                Toast.LENGTH_LONG).show();;
                    }
                } else {
                    //Don't do something
                    Toast.makeText(TransactionActivity.this,
                            "Error",
                            Toast.LENGTH_LONG).show();
                }
            }
    );

    public void bttDisplay(){

        //TRANSACTION PRINTER
        final int[] checkedItem = {-1};
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Choose an additional item to add to this event:");

        /* Generate list from BTT types */
        String[] keys = new BizTransactionType()
                .getTypes()
                .keySet()
                .toArray(new String[0]);
        String[] listItems = new BizTransactionType()
                .getTypes()
                .values()
                .toArray(new String[0]);

        alertDialog.setSingleChoiceItems(listItems, checkedItem[0], (dialog, which) -> {

            checkedItem[0] = which;

            bttType.add(k, keys[which]);

            Alert.showAlertDialog(this,"Adding "+listItems[which],
                    "Scan QR to add business transaction reference to this event.",
                    (dialog2, which2) -> {
                        if ((which2 == DialogInterface.BUTTON_POSITIVE))
                            initBTTScan();
                    });
            dialog.dismiss();
        });

        alertDialog.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
        AlertDialog customAlertDialog = alertDialog.create();
        customAlertDialog.show();
    }

    public void initBTTScan(){
        Intent intent = new Intent(TransactionActivity.this, ScanActivity.class);
        BTTcapture.launch(intent);
    }

    ActivityResultLauncher<Intent> BTTcapture = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), activityResult -> {
                int result = activityResult.getResultCode();
                Intent data = activityResult.getData();

                if (result == RESULT_OK) {

                    //Do something
                    if (data != null) {

                        format = data.getStringExtra("format");
                        scanResults = data.getStringExtra("content");

                        //Check if the scan is of QR_CODE format.
                        if (!format.equals("QR_CODE")) {
                            //Currently unsupported format.
                            String error = "Currently only QR codes are supported.";
                            Dialog dialog = new AlertDialog.Builder(this)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setTitle("Error")
                                    .setMessage(error)
                                    .setPositiveButton("Ok", (dialog2, which) -> initBTTScan())
                                    .show();
                            dialog.setCanceledOnTouchOutside(false);
                        } else {

                            //PARSE SCAN RESULTS FOR URL
                            if (URLUtil.isValidUrl(scanResults) && scanResults.endsWith(".pdf")) {
                                Dialog dialog;
                                dialog = new AlertDialog.Builder(this)
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setTitle("Document link detected!")
                                        .setMessage(scanResults + "\nThis is a link to a document." +
                                                "\nDo you want to create a validation check sum?")
                                        .setPositiveButton("Yes", (dialog2, which) ->{
                                            adapter_other.clear();
                                            validateDoc(scanResults);
                                            bttValue.add(k, scanResults);
                                            checkSum = null;
                                            epcList_other.add(scanResults);
                                            for (int i = 0; i < epcList_other.size(); ++i)
                                                adapter_other.add(epcList_other.get(i));
                                            ++k;
                                            EPClist_other.setAdapter(adapter_other);

                                        })
                                        .setNegativeButton("No", (dialog2, which) -> {

                                        })
                                        .show();
                                dialog.setCanceledOnTouchOutside(false);
                            } else {
                                Dialog dialog3 = new AlertDialog.Builder(this)
                                        .setIcon(android.R.drawable.ic_dialog_info)
                                        .setTitle("Scan Result")
                                        .setMessage("Type: "+format+"\nContent: "+scanResults)
                                        .setNegativeButton("Rescan", (dialog4, which2) -> {
                                            Toast.makeText(getApplicationContext(),
                                                    "Rescanning...",Toast.LENGTH_SHORT)
                                                    .show();
                                            //Rescan
                                            initBTTScan();
                                        })
                                        .setNeutralButton("Finish", (dialog4, which2) -> {
                                            adapter_other.clear();
                                            bttValue.add(k, scanResults);
                                            epcList_other.add(scanResults);
                                            for (int i = 0; i < epcList_other.size(); ++i)
                                                adapter_other.add(epcList_other.get(i));
                                            ++k;
                                            EPClist_other.setAdapter(adapter_other);
                                        })
                                        .show();
                                dialog3.setCanceledOnTouchOutside(false);
                                dialog3.setOnCancelListener(dialog1 ->
                                        epcList_other.remove(epcList_other.size() - 1));
                            }
                        }

                    } else {
                        Toast.makeText(TransactionActivity.this, "Error",
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    //Don't do something
                    Toast.makeText(TransactionActivity.this,
                            "Error",
                            Toast.LENGTH_LONG).show();
                }
            }
    );

    public void validateDoc(String url){
        Intent intent = new Intent(TransactionActivity.this, ValidationActivity.class);
        intent.putExtra("url", url);
        docCapture.launch(intent);
    }

    ActivityResultLauncher<Intent> docCapture = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), activityResult -> {
                int result = activityResult.getResultCode();
                Intent data = activityResult.getData();

                if (result == RESULT_OK) {
                    //Do something
                    if (data != null) {
                        checkSum = data.getStringExtra("checksum");
                        Log.d("Checksum",checkSum);
                        bttChecksum.put(scanResults, checkSum);
                    }
                }
            }
    );



    private void buildEvent() throws JSONException, NoSuchAlgorithmException {

        transactionEvent = new TransactionEvent();
        transactionEvent.setAction(this.getIntent().getStringExtra("ACTION"));
        transactionEvent.setBizStep(new BizStepType(this.getIntent().getStringExtra("BIZSTEP")));
        transactionEvent.setDisposition(this.getIntent().getStringExtra("DISPOSITION"));
        transactionEvent.setBizLocation(this.getIntent().getStringExtra("BIZLOCATION"));
        transactionEvent.setReadPoint(this.getIntent().getStringExtra("READPOINT"));
        transactionEvent.setEventTimeZoneOffset();
        transactionEvent.setEpcList(epcList);
        //transformEvent.setQuantityList(quantityList);

        //Build biz transactions
        if(k > 0) {
            List<HashMap<String,String>> map = new ArrayList<>();
            for (int i = 0; i < k; ++i){
                HashMap<String, String> bttMap = new HashMap<>();
                bttMap.put("type", bttType.get(i));
                bttMap.put("bizTransaction", bttValue.get(i));
                Log.d("Checksum on "+bttValue.get(i), bttChecksum.get(bttValue.get(i)));
                if(bttChecksum.get(bttValue.get(i)) != null){
                    Log.d("Checksum on "+bttValue.get(i), bttChecksum.get(bttValue.get(i)));
                    bttMap.put("checksum", bttChecksum.get(bttValue.get(i)));
                }

                map.add(bttMap);
            }
            transactionEvent.setBizTransactionList(map);
        }

        //Hash the value of the event to generate a unique ID.
        transactionEvent.setEventId();

        //Attempt to pass the object into JSON format.
        try {
            epcisObj = transactionEvent.jsonObject();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String parseGTIN(String gtin, String serial){

        //Start example: 09506000134352 9999
        //Converted: 9506000.013435.9999

        //Drop check digit
        gtin = gtin.substring(0,13);

        //Grab and move indicator
        return "urn:epc:id:sgtin:"
                +gtin.substring(1,8)+"."
                +gtin.charAt(0)
                +gtin.substring(8,13)
                +"."+serial;
    }

    private String parseSSCC(String r) {

        //Check digit precedes prefix at 2nd index
        char check = r.charAt(2);

        //Company prefix is 8 digits starting from 3rd index
        String pref = r.substring(3,10);

        //Serialization is all remaining characters.
        String serial = r.substring(10);

        return "urn:epc:id:sscc:"
                +pref+"."
                +check+"."
                +serial;
    }

    private void postActivity() throws JSONException {

        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle("Posting")
                .setMessage("Submitting scan ativity of ("+quantityList.size()+") scans?")
                .setPositiveButton("Yes", (dialog, which) -> {

                    //HttpPostRequest postRequest = new HttpPostRequest();
                    //postRequest.postActivity(this, epcisObj);

                    // TODO: remove debugger
                    //DEBUG PRINTER
                    final TextView input = new TextView (this);
                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
                    JsonParser jp = new JsonParser();
                    JsonElement je = jp.parse(String.valueOf(epcisObj));
                    String prettyJsonString = gson.toJson(je);
                    input.setText(prettyJsonString);
                    input.setTextSize(12);
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setView(input);
                    alert.setPositiveButton("Ok",(dialog2, which2) -> {
                        HttpPostRequest postRequest = new HttpPostRequest();
                        postRequest.postActivity(this, epcisObj);
                        restart();
                    });
                    alert.setNegativeButton("Cancel", null);
                    alert.show();
                    //END DEBUG PRINTER
                })
                .setNegativeButton("No", (dialog, which) -> restart())
                .show();
    }

    public void restart(){
        reset();
    }

    public void reset(){

        //Reset the epcis/obj events
        quantityList.clear();
        epcList.clear();
        epcList_other.clear();

        //Rest business transaction events.
        bttType.clear();
        bttValue.clear();
        k = 0;

        //Clear objects
        epcisObj = null;
        transactionEvent = null;

        //Reset views
        adapter_other.clear();
        adapter.clear();
        EPClist.removeAllViewsInLayout();
        EPClist_other.removeAllViewsInLayout();

        //Disable buttons
        btnSubmit.setEnabled(false);
        btnRem.setEnabled(false);
    }

    @Override
    public void onBackPressed(){
        Alert.showAlertDialog(this,"Exiting!",
                "This will reset the device configuration.\n\n"+
                        "Are you sure you want to proceed?", (dialog, which) -> {
                    if ((which == DialogInterface.BUTTON_POSITIVE))
                        finish();
                });
    }

}
