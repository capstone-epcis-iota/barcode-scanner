package com.scanner.activity;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.scanner.Alert;
import com.scanner.BuildConfig;
import com.scanner.Checksum;
import com.scanner.FileDownloader;
import com.scanner.R;
import com.scanner.cbv.EventId;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class ValidationActivity extends AppCompatActivity implements View.OnClickListener {

    String TAG = "VALIDATIONACTIVITY";
    private static final String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
    String url;
    String filename;
    String id;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validation);

        setTitle("Schroedinger Motors EPC Scanner");
        Log.v(TAG, "onCreate() Method invoked ");

        ActivityCompat.requestPermissions(ValidationActivity.this, PERMISSIONS, 112);
        request();

        if(hasPermissions(ValidationActivity.this, PERMISSIONS)){
            url = getIntent().getStringExtra("url");
            filename = "test.pdf";
            if(URLUtil.isValidUrl(url)){
                download();
                //view();
                try {
                    hash();

                    Intent results = new Intent ();
                    results.putExtra("checksum", id);
                    setResult(RESULT_OK, results);

                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            Dialog dialog = new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Error")
                    .setMessage("Error with download")
                    .setPositiveButton("Ok", (dialog2, which) -> finish())
                    .show();
            dialog.setCanceledOnTouchOutside(false);
        }
    }

    @Override
    public void onClick(View v) {

    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission)
                        != PackageManager.PERMISSION_GRANTED) {
                    return true;
                }
            }
        }
        return false;
    }

    public void request() {
        ActivityCompat.requestPermissions(
                ValidationActivity.this, PERMISSIONS, 112);
    }

    public void download() {
        Log.v(TAG, "download() Method invoked ");
        new DownloadFile().execute(url, filename);
        Log.v(TAG, "download() Method completed ");
    }

    public void hash() throws Exception {
        Log.v(TAG, "hash() Method invoked ");

        File d = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File pdfFile = new File(d, filename);

        Log.v(TAG, "hash() Method pdfFile " + pdfFile.getAbsolutePath());

        Uri path = FileProvider.getUriForFile(this,
                BuildConfig.APPLICATION_ID + ".provider", pdfFile);

        Log.v(TAG, "hash() Method path " + path);

        Checksum check = new Checksum();
        check.setId(pdfFile);
        id = check.getId();

        //delete file when done
        if(pdfFile.delete()){
            Log.v(TAG, "hash() Method completed - file deleted");
        } else {
            Log.v(TAG, "hash() Method completed - unable to delete file");
        }

        Log.v(TAG, "hash() Method completed");
        Log.d(TAG, "hash()+\n"+check.getId());
    }

    public void view() {
        Log.v(TAG, "view() Method invoked ");

        File d = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File pdfFile = new File(d, filename);

        Log.v(TAG, "view() Method pdfFile " + pdfFile.getAbsolutePath());

        Uri path = FileProvider.getUriForFile(this,
                BuildConfig.APPLICATION_ID + ".provider", pdfFile);

        Log.v(TAG, "view() Method path " + path);

        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        try {
            startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(ValidationActivity.this,
                    "No Application available to view PDF", Toast.LENGTH_SHORT).show();
        }

        Log.v(TAG, "view() Method completed ");
    }

    private static class DownloadFile extends AsyncTask<String, Void, Void> {

        String TAG = "VALIDATIONACTIVITY";

        @Override
        protected Void doInBackground(String... strings) {
            Log.v(TAG, "doInBackground() Method invoked ");

            String fileUrl = strings[0];
            String fileName = strings[1];
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

            File pdfFile = new File(folder, fileName);
            Log.v(TAG, "doInBackground() pdfFile invoked " + pdfFile.getAbsolutePath());
            Log.v(TAG, "doInBackground() pdfFile invoked " + pdfFile.getAbsoluteFile());

            try {
                pdfFile.createNewFile();
                Log.v(TAG, "doInBackground() file created" + pdfFile);

            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, "doInBackground() error" + e.getMessage());
                Log.e(TAG, "doInBackground() error" + Arrays.toString(e.getStackTrace()));

            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            Log.v(TAG, "doInBackground() file download completed");

            return null;
        }
    }

    @Override
    public void onBackPressed(){
        Alert.showAlertDialog(this,"Exiting!",
                "This will reset the device configuration.\n\n"+
                        "Are you sure you want to proceed?", (dialog, which) -> {
                    if ((which == DialogInterface.BUTTON_POSITIVE))
                        finish();
                });
    }
}
