package com.scanner.cbv;

public class ActionType {

    protected final String[] types = {
            "OBSERVE",
            "ADD",
            "DELETE"
    };

    String type;

    public ActionType(String type) {
        this.type = type;
    }

    public void setType (String type) {
        this.type = type;
    }

    public String getType () {
        return this.type;
    }
}
