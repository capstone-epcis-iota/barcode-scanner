package com.scanner.cbv;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Arrays;
import java.util.List;

public class BizStepType extends AppCompatActivity {

    protected final String[] types = {
            "accepting",
            "arriving",
            "assembling",
            "collecting",
            "commissioning",
            "consigning",
            "creating_class_instance",
            "cycle_counting",
            "decommissioning",
            "departing",
            "destroying",
            "disassembling",
            "dispensing",
            "encoding",
            "entering_exiting",
            "holding",
            "inspecting",
            "installing",
            "killing",
            "loading",
            "other",
            "packing",
            "picking",
            "receiving",
            "removing",
            "repackaging",
            "repairing",
            "replacing",
            "reserving",
            "retail_selling",
            "sampling",
            "sensor_reporting",
            "shipping",
            "staging_outbound",
            "stock_taking",
            "stocking",
            "storing",
            "transporting",
            "unloading",
            "unpacking",
            "void_shipping"
    };

    String type;

    public BizStepType(String type){
        if (isValid(type)) {
            this.type = "urn:epcglobal:cbv:bizstep:"+type;
        } else {
            System.err.println(type+"\nInvalid BizStepType");
        }

    }

    public void setType (String type) {
        if (isValid(type)) {
            this.type = "urn:epcglobal:cbv:bizstep:"+type;
        } else {
            Toast.makeText(this, "Invalid Configuration File",Toast.LENGTH_SHORT).show();
            System.err.println(type+"\nInvalid BizStepType");
        }
    }

    public String getType () {
        return this.type;
    }

    public Boolean isValid(String t){
        List<String> list = Arrays.asList(types);
        return list.contains(t);
    }
}
