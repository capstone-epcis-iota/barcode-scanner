package com.scanner.cbv;

import java.util.HashMap;

public class BizTransactionType {

    protected HashMap<String, String> types;
    protected String type;
    protected String checkSum;

    public BizTransactionType() {
        type = null;

        types = new HashMap<String, String>() {{
        /* Bill of Lading. A document issued by a carrier to a shipper,
        listing and acknowledging receipt of goods or transport and
        specifying terms of delivery */
            put("bol", "Bill of Lading");

        /* Purchase Order Confirmation. A document
        that provides confirmation from an external supplier to the
        request of a purchaser to deliver a specified quantity of
        material, or perform a specified service, at a specified price
        within a specified time. */
            put("po", "Purchase Order");

        /*Purchase Order Confirmation. A document
        that provides confirmation from an external supplier to the
        request of a purchaser to deliver a specified quantity of
        material, or perform a specified service, at a specified price
        within a specified time.*/
            put("poc", "Purchase Order Confirmation");

        /* "A document confirming certain characteristics of an object
        (e.g. product), person, or organisation, typically issued
        by a third party." */
            put("cert", "Certificate");

        /* Despatch Advice. A document/message by
		means of which the seller or consignor informs the consignee
	    about the despatch of goods. Also called an “Advanced Shipment
		Notice,” but the value desadv is always used regardless of local
		nomenclature. */
            put("desadv", "Dispatch Advice");

        /*A document/message claiming
	    payment for goods or services supplied under conditions agreed by
		the seller and buyer.*/
            put("inv", "Invoice");

        /*Pedigree. A record that traces the
		ownership or custody and transactions of a product as it moves
		among various trading partners.*/
            put("pedigree", "Pedigree");

        /*Production Order. An organisation-internal
	    document or message issued by a producer that initiates a
		manufacturing process of goods.*/
            put("prodorder", "Production Order");

        /* Receiving Advice. A document/message that
		provides the receiver of the shipment the capability to inform
		the shipper of actual goods received, compared to what was
		advised as being sent. */
            put("recadv", "Receiving Advice");

        /* Return Merchandise Authorisation. A
		document issued by the seller that authorises a buyer to return
	    merchandise for credit determination.*/
            put("rma", "Return Merchandise Authorization");

        /* A document that provides a formal specification of a
        sequence of instructions for the purpose of verifying one or
        several criteria. */
            put("testprd", "Test Procedure");

        /* A document that includes the outcome of the execution
        of a given test procedure. */
            put("testres", "Test Result");

        /* Event ID URI(s) of event(s) provided by an upstream supplier,
        such as packing and shipping events (e.g., as the basis for
        the inferred completeness of inbound aggregations). */
            put("upevt", "Upstream EPCIS Event");
        }};
    }

    public BizTransactionType(String type){
        if (isValid(type)) {
            this.type = "urn:epcglobal:cbv:BTT-"+type;
        } else {
            System.err.println(type+"\nInvalid BizStepType");
        }
    }

    public HashMap<String, String> getTypes () {
        return types;
    }

    public void setType (String type) {
        if (isValid(type)) {
            this.type = "urn:epcglobal:cbv:BTT-"+type;
        } else {
            System.err.println(type+"\nInvalid BizTransactionType");
        }
    }

    public void setChecksum(String check) {
        this.checkSum = check;
    }

    public String getCheckSum() {
        return this.checkSum;
    }

    public String getType () {
        return this.type;
    }

    public Boolean isValid(String t){
        return types.containsKey(t);
    }

}
