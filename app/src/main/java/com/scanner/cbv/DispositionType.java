package com.scanner.cbv;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class DispositionType {

    private final String [] types = {
            "active",
            "available",
            "completeness_verified",
            "completeness_inferred",
            "conformant",
            "container_closed",
            "container_open",
            "damaged",
            "destroyed",
            "dispensed",
            "disposed",
            "encoded",
            "expired",
            "in_progress",
            "in_transit",
            "inactive",
            "mismatch_instance",
            "mismatch_class",
            "mismatch_quantity",
            "needs_replacement",
            "non_conformant",
            "non_sellable_other",
            "partially_dispensed",
            "recalled",
            "reserved",
            "retail_sold",
            "returned",
            "sellable_accessible",
            "sellable_not_accessible",
            "stolen",
            "unavailable",
            "unknown"
    };

    String type;

    public DispositionType(String type){
        if (isValid(type)) {
            this.type = "urn:epcglobal:cbv:disp:"+type;
        } else {
            System.err.println("Invalid Disposition");
        }

    }

    public void setType (String type) {
        if (isValid(type)) {
            this.type = "urn:epcglobal:cbv:disp:"+type;
        } else {
            System.err.println("Invalid Disposition");
        }
    }

    public String getType () {
        return this.type;
    }

    public Boolean isValid(String t){
        List<String> list = Arrays.asList(types);
        return list.contains(t.toLowerCase(Locale.ROOT));
    }
}
