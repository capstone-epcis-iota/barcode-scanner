package com.scanner.cbv;

import com.scanner.event.EPCISEvent;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EventId {

    static String encoding;
    static String version;
    String id;

    public EventId(){
        this.id = null;
    }

    public void setId(EPCISEvent event) throws NoSuchAlgorithmException {
        String eventToString = event.toString();
        this.id = toHexString(getSHA(eventToString));
    }

    public String getId (){
        return encoding+this.id+version;
    }

    public static byte[] getSHA(String input) throws NoSuchAlgorithmException
    {
        // Static getInstance method is called with hashing SHA
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        encoding = "ni:///sha-256;";
        version = "?ver=CBV2.0";

        // digest() method called
        // to calculate message digest of an input
        // and return array of byte
        return md.digest(input.getBytes(StandardCharsets.UTF_8));
    }

    public static String toHexString(byte[] hash)
    {
        // Convert byte array into signum representation
        BigInteger number = new BigInteger(1, hash);

        // Convert message digest into hex value
        StringBuilder hexString = new StringBuilder(number.toString(16));

        // Pad with leading zeros
        while (hexString.length() < 32)
        {
            hexString.insert(0, '0');
        }

        return hexString.toString();
    }
}
