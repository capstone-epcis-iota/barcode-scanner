package com.scanner.cbv;

public class Location {

    String location;

    public Location(String location){
        this.location = "urn:epc:id:sgln:"+location;

    }

    public void setType (String loc) {
        this.location = loc;
    }

    public String getReadpoint () {
        return this.location;
    }
}
