package com.scanner.cbv;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class SourceDestinationType {

    private final String [] types = {
        "owning_party",
        "possessing_party",
        "location"
    };

    String type;

    public SourceDestinationType(String type){
        if (isValid(type)) {
            this.type = "urn:epcglobal:cbv:sdt:"+type;
        } else {
            System.err.println("Invalid Source Destination Type");
        }

    }

    public void setType (String type) {
        if (isValid(type)) {
            this.type = "urn:epcglobal:cbv:sdt:"+type;
        } else {
            System.err.println("Invalid Source Destination Type");
        }
    }

    public String getType () {
        return this.type;
    }

    public Boolean isValid(String t){
        List<String> list = Arrays.asList(types);
        return list.contains(t.toLowerCase(Locale.ROOT));
    }
}
