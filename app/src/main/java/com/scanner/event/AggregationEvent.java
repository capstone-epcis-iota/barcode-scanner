package com.scanner.event;

import android.os.Parcel;
import android.os.Parcelable;

import com.scanner.CaptureUtil;
import com.scanner.cbv.BizStepType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AggregationEvent extends EPCISEvent implements Parcelable {

    // EventTime, EventTimeZoneOffset,Action required
    private String action;

    private String parentID;
    private List<String> childEPCs;
    private List<Integer> childQuantityList;

    private BizStepType bizStep;
    private String disposition;
    private String readPoint;
    private String bizLocation;
    private List<HashMap<String,String>> bizTransactionList;
    private Map<String, List<String>> sourceList;
    private Map<String, List<String>> destinationList;
    private Map<String, String> namespaces;

    public AggregationEvent() {
        super();

        action = "null";
        childEPCs = new ArrayList<>();
        childQuantityList = new ArrayList<>();
        bizTransactionList = new ArrayList<>();
        sourceList = new HashMap<>();
        destinationList = new HashMap<>();
        namespaces = new HashMap<>();
    }

    protected AggregationEvent(Parcel in) {
        action = in.readString();
        parentID = in.readString();
        childEPCs = in.createStringArrayList();
        bizStep = new BizStepType(in.readString());
        disposition = in.readString();
        readPoint = in.readString();
        bizLocation = in.readString();
    }

    public static final Creator<AggregationEvent> CREATOR = new Creator<AggregationEvent>() {
        @Override
        public AggregationEvent createFromParcel(Parcel in) {
            return new AggregationEvent(in);
        }

        @Override
        public AggregationEvent[] newArray(int size) {
            return new AggregationEvent[size];
        }
    };

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public BizStepType getBizStep() {
        return bizStep;
    }

    public void setBizStep(BizStepType bizStep) {
        this.bizStep = bizStep;
    }

    public String getDisposition() {
        return disposition;
    }

    public void setDisposition(String disposition) {
        this.disposition = disposition;
    }

    public String getReadPoint() {
        return readPoint;
    }

    public void setReadPoint(String readPoint) {
        this.readPoint = readPoint;
    }

    public String getBizLocation() {
        return bizLocation;
    }

    public void setBizLocation(String bizLocation) {
        this.bizLocation = bizLocation;
    }

    public List<HashMap<String,String>> getBizTransactionList(){
        return bizTransactionList;
    }

    public void setBizTransactionList(List<HashMap<String,String>> bizTransactionList){
        this.bizTransactionList = bizTransactionList;
    }

    public Map<String, List<String>> getSourceList() {
        return sourceList;
    }

    public void setSourceList(Map<String, List<String>> sourceList) {
        this.sourceList = sourceList;
    }

    public Map<String, List<String>> getDestinationList() {
        return destinationList;
    }

    public void setDestinationList(Map<String, List<String>> destinationList) {
        this.destinationList = destinationList;
    }

    public Map<String, String> getNamespaces() {
        return namespaces;
    }

    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

    public List<String> getChildEPCs() {
        return childEPCs;
    }

    public void setChildEPCs(List<String> childEPCs) {
        this.childEPCs = childEPCs;
    }

    public List<Integer> getChildQuantityList() {
        return childQuantityList;
    }

    public void setChildQuantityList(List<Integer> childQuantityList) {
        this.childQuantityList = childQuantityList;
    }

    public void setNamespaces(Map<String, String> namespaces) {
        this.namespaces = namespaces;
    }

    public JSONObject jsonObject() throws JSONException {
        CaptureUtil util = new CaptureUtil();

        JSONObject aggregationEvent = super.jsonObject();

        // Required Fields
        aggregationEvent = util.putEventType(aggregationEvent, "AggregationEvent");
        aggregationEvent = util.putAction(aggregationEvent, action);

        // Optional Fields
        if (this.parentID != null) {
            aggregationEvent = util.putParentID(aggregationEvent, parentID);
        }
        if (this.childEPCs != null && this.childEPCs.size() != 0) {
            aggregationEvent = util.putChildEPCs(aggregationEvent, childEPCs);
        }
        if (this.bizStep != null) {
            aggregationEvent = util.putBizStep(aggregationEvent, bizStep);
        }
        if (this.disposition != null) {
            aggregationEvent = util.putDisposition(aggregationEvent, disposition);
        }
        if (this.readPoint != null) {
            aggregationEvent = util.putReadPoint(aggregationEvent, readPoint);
        }
        if (this.bizLocation != null) {
            aggregationEvent = util.putBizLocation(aggregationEvent, bizLocation);
        }
        if (this.bizTransactionList != null && !this.bizTransactionList.isEmpty()) {
            aggregationEvent = util.putBizTransactionList(aggregationEvent, bizTransactionList);
        }
        if (this.bizLocation != null) {
            aggregationEvent = util.putBizLocation(aggregationEvent, bizLocation);
            return aggregationEvent;
        }
        return aggregationEvent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(action);
        dest.writeString(parentID);
        dest.writeStringList(childEPCs);
        dest.writeString(bizStep.getType());
        dest.writeString(disposition);
        dest.writeString(readPoint);
        dest.writeString(bizLocation);
    }
}
