package com.scanner.event;

import com.scanner.CaptureUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class EPCISDocument {

    private final String eventType;
    private String schemaVersion;
    private String creationDate;
    private String format;
    private final String context;
    private final List<EPCISEvent> epcisBody = new ArrayList<>();
    private final JSONObject readpoint = new JSONObject();

    String pattern = "yyyy-MM-dd'T'HH:mm:ssZZZZ";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.ENGLISH);

    public EPCISDocument() throws JSONException {
        JSONObject rp = new JSONObject();
        rp.put("id", "123");
        readpoint.put("readpoint", rp);
        eventType = "EPCISDocument";
        schemaVersion = null;
        context = null;
        creationDate = simpleDateFormat.format(new Date());
        format = null;
    }

    public EPCISDocument(String sv, String format, String context) {
        eventType = "EPCISDocument";
        creationDate = simpleDateFormat.format(new Date());
        this.schemaVersion = sv;
        this.format = format;
        this.context = context;
    }

    public String getSchemaVersion(){
        return this.schemaVersion;
    }

    public void setSchemaVersion(String schemaVersion){
        this.schemaVersion = schemaVersion;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getFormat(){ return this.format; }

    public void setFormat(String format) { this.format = format; }

    public List<EPCISEvent> getEpcisBody(){ return this.epcisBody; }

    public void setEpcisBody(EPCISEvent epcisEvent) {
        this.epcisBody.add(epcisEvent); }

    public JSONObject jsonObject() throws JSONException {
        CaptureUtil util = new CaptureUtil();

        JSONObject baseDoc = new JSONObject();
        // Required Fields
        baseDoc = util.putEventType(baseDoc, eventType);
        baseDoc = util.putSchemaVersion(baseDoc, schemaVersion);
        baseDoc = util.putCreationDate(baseDoc, creationDate);
        baseDoc = util.putFormat(baseDoc, format);
        baseDoc = util.putEPCISBody(baseDoc, epcisBody);
        baseDoc = util.putContext(baseDoc, context);

        baseDoc = util.putReadPoint(baseDoc, "123");

        return baseDoc;
    }

}
