package com.scanner.event;

import com.scanner.CaptureUtil;
import com.scanner.cbv.EventId;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public abstract class EPCISEvent extends JSONObject {

    private EventId eventId;
    private String eventTime;
    private String eventTimeZoneOffset;
    private String context;

    String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSXXX";
    SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, Locale.ENGLISH);

    public EPCISEvent() {
        eventId = new EventId();
        eventTime = dateFormat.format(new Date());
        setEventTimeZoneOffset();
        context = "https://id.gs1.org/epcis-context.jsonld";
    }

    public void setEventId() throws NoSuchAlgorithmException {
        eventId.setId(this);
    }

    public String getEventId() {
        return this.eventId.getId();
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventTimeZoneOffset() {
        return eventTimeZoneOffset;
    }

    public void setEventTimeZoneOffset() {
        Calendar c = Calendar.getInstance();
        TimeZone tz = c.getTimeZone();

        eventTimeZoneOffset = ZonedDateTime.now(tz.toZoneId())
                .getOffset()
                .getId();
    }

    public void setEventTimeZoneOffset(String eventTimeZoneOffset) {
        this.eventTimeZoneOffset = eventTimeZoneOffset;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getContext () {
        return this.context;
    }

    public JSONObject jsonObject() throws JSONException {
        CaptureUtil util = new CaptureUtil();

        JSONObject baseEvent = new JSONObject();

        // Required Fields
        baseEvent = util.putEventId(baseEvent, eventId.getId());
        baseEvent = util.putEventTime(baseEvent, eventTime);
        baseEvent = util.putEventTimeZoneOffset(baseEvent, eventTimeZoneOffset);
        baseEvent = util.putContext(baseEvent, context);

        return baseEvent;
    }
}