package com.scanner.event;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.scanner.CaptureUtil;
import com.scanner.cbv.BizStepType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ObjectEvent extends EPCISEvent implements Parcelable {

    // EventTime, EventTimeZoneOffset, Action required
    private List<String> epcList;
    private List<Integer> quantityList;
    private String action;
    private BizStepType bizStep;
    private String disposition;
    private String readPoint;
    private String bizLocation;
    private List<HashMap<String,String>> bizTransactionList;
    private Map<String, List<String>> sourceList;
    private Map<String, List<String>> destinationList;

    public ObjectEvent() {
        super();

        action = null;
        epcList = new ArrayList<>();
        quantityList = new ArrayList<>();
        bizTransactionList = new ArrayList<>();
        sourceList = new HashMap<>();
        destinationList = new HashMap<>();
    }

    protected ObjectEvent(Parcel in) {
        epcList = in.createStringArrayList();
        action = in.readString();
        disposition = in.readString();
        bizStep = new BizStepType(in.readString());
        readPoint = in.readString();
        bizLocation = in.readString();
    }

    public static final Creator<ObjectEvent> CREATOR = new Creator<ObjectEvent>() {
        @Override
        public ObjectEvent createFromParcel(Parcel in) {
            return new ObjectEvent(in);
        }

        @Override
        public ObjectEvent[] newArray(int size) {
            return new ObjectEvent[size];
        }
    };

    public List<String> getEpcList() {
        return epcList;
    }

    public void setEpcList(List<String> epcList) {
        this.epcList = epcList;
    }

    public List<Integer> getQuantityList() {
        return quantityList;
    }

    public void setQuantityList(List<Integer> quantityList) {
        this.quantityList = quantityList;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getBizStep() {
        return bizStep.getType();
    }

    public void setBizStep(BizStepType bizStep) {
        this.bizStep = bizStep;
    }

    public String getDisposition() {
        return disposition;
    }

    public void setDisposition(String disposition) {
        this.disposition = disposition;
    }

    public String getReadPoint() {
        return readPoint;
    }

    public void setReadPoint(String readPoint) {
        this.readPoint = readPoint;
    }

    public String getBizLocation() {
        return bizLocation;
    }

    public void setBizLocation(String bizLocation) {
        this.bizLocation = bizLocation;
    }

    public List<HashMap<String,String>> getBizTransactionList(){
        return bizTransactionList;
    }

    public void setBizTransactionList(List<HashMap<String,String>> bizTransactionList){
        this.bizTransactionList = bizTransactionList;
    }

    public Map<String, List<String>> getSourceList() {
        return sourceList;
    }

    public void setSourceList(Map<String, List<String>> sourceList) {
        this.sourceList = sourceList;
    }

    public Map<String, List<String>> getDestinationList() {
        return destinationList;
    }

    public void setDestinationList(Map<String, List<String>> destinationList) {
        this.destinationList = destinationList;
    }

    public JSONObject jsonObject() throws JSONException {
        Log.d("objectEvent", "Creating JSON");
        CaptureUtil util = new CaptureUtil();
        JSONObject objectEvent = super.jsonObject();

        // Required Fields
        objectEvent = util.putEventType(objectEvent, "ObjectEvent");
        objectEvent = util.putAction(objectEvent, action);

        // Optional Fields
        if (this.epcList != null && this.epcList.size() != 0) {
            objectEvent = util.putEPCList(objectEvent, epcList);
        }
        if (this.bizStep != null) {
            objectEvent = util.putBizStep(objectEvent, bizStep);
        }
        if (this.disposition != null) {
            objectEvent = util.putDisposition(objectEvent, disposition);
        }
        if (this.readPoint != null) {
            objectEvent = util.putReadPoint(objectEvent, readPoint);
        }
        if (this.bizLocation != null) {
            objectEvent = util.putBizLocation(objectEvent, bizLocation);
        }
        if (this.bizTransactionList != null && !this.bizTransactionList.isEmpty()) {
            objectEvent = util.putBizTransactionList(objectEvent, bizTransactionList);
        }

        return objectEvent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(epcList);
        dest.writeString(action);
        dest.writeString(disposition);
        dest.writeString(readPoint);
        dest.writeString(bizLocation);
        dest.writeString(String.valueOf(bizTransactionList));
    }
}