package com.scanner.event;

import com.scanner.CaptureUtil;
import com.scanner.cbv.BizStepType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransactionEvent extends EPCISEvent {

    // EventTime, EventTimeZoneOffset,Action, bizTransactionList required
    private String action;
    private List<HashMap<String,String>> bizTransactionList;

    private String parentID;
    private List<String> epcList;
    private List<Integer> quantityList;

    private BizStepType bizStep;
    private String disposition;
    private String readPoint;
    private String bizLocation;

    private Map<String, List<String>> sourceList;
    private Map<String, List<String>> destinationList;

    public TransactionEvent() {
        super();

        action = null;
        epcList = new ArrayList<>();
        quantityList = new ArrayList<>();
        bizTransactionList = new ArrayList<>();
        sourceList = new HashMap<>();
        destinationList = new HashMap<>();
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public List<HashMap<String,String>> getBizTransactionList(){
        return bizTransactionList;
    }

    public void setBizTransactionList(List<HashMap<String,String>> bizTransactionList){
        this.bizTransactionList = bizTransactionList;
    }

    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

    public List<String> getEpcList() {
        return epcList;
    }

    public void setEpcList(List<String> epcList) {
        this.epcList = epcList;
    }

    public List<Integer> getQuantityList() {
        return quantityList;
    }

    public void setQuantityList(List<Integer> quantityList) {
        this.quantityList = quantityList;
    }

    public BizStepType getBizStep() {
        return bizStep;
    }

    public void setBizStep(BizStepType bizStep) {
        this.bizStep = bizStep;
    }

    public String getDisposition() {
        return disposition;
    }

    public void setDisposition(String disposition) {
        this.disposition = disposition;
    }

    public String getReadPoint() {
        return readPoint;
    }

    public void setReadPoint(String readPoint) {
        this.readPoint = readPoint;
    }

    public String getBizLocation() {
        return bizLocation;
    }

    public void setBizLocation(String bizLocation) {
        this.bizLocation = bizLocation;
    }

    public Map<String, List<String>> getSourceList() {
        return sourceList;
    }

    public void setSourceList(Map<String, List<String>> sourceList) {
        this.sourceList = sourceList;
    }

    public Map<String, List<String>> getDestinationList() {
        return destinationList;
    }

    public void setDestinationList(Map<String, List<String>> destinationList) {
        this.destinationList = destinationList;
    }

    public JSONObject jsonObject() throws JSONException {
        CaptureUtil util = new CaptureUtil();

        JSONObject transactionEvent = super.jsonObject();

        // Required Fields
        transactionEvent = util.putEventType(transactionEvent, "TransactionEvent");
        transactionEvent = util.putAction(transactionEvent, action);
        transactionEvent = util.putBizTransactionList(transactionEvent, bizTransactionList);

        // Optional Fields
        if (this.parentID != null) {
            transactionEvent = util.putParentID(transactionEvent, parentID);
        }
        if (this.epcList != null && this.epcList.size() != 0) {
            transactionEvent = util.putEPCList(transactionEvent, epcList);
        }
        if (this.bizStep != null) {
            transactionEvent = util.putBizStep(transactionEvent, bizStep);
        }
        if (this.disposition != null) {
            transactionEvent = util.putDisposition(transactionEvent, disposition);
        }
        if (this.readPoint != null) {
            transactionEvent = util.putReadPoint(transactionEvent, readPoint);
        }
        if (this.bizLocation != null) {
            transactionEvent = util.putBizLocation(transactionEvent, bizLocation);
        }

        return transactionEvent;
    }
}