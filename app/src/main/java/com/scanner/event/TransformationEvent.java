package com.scanner.event;

import com.scanner.CaptureUtil;
import com.scanner.cbv.BizStepType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransformationEvent extends EPCISEvent {

    // EventTime, EventTimeZoneOffset required
    private List<String> inputEPCList;
    private List<Integer> inputQuantityList;
    private List<String> outputEPCList;
    private List<Integer> outputQuantityList;

    private String transformationID;

    private BizStepType bizStep;
    private String disposition;
    private String readPoint;
    private String bizLocation;

    private List<HashMap<String,String>> bizTransactionList;
    private Map<String, List<String>> sourceList;
    private Map<String, List<String>> destinationList;

    public TransformationEvent() {
        super();

        inputEPCList = new ArrayList<>();
        inputQuantityList = new ArrayList<>();
        outputEPCList = new ArrayList<>();
        outputQuantityList = new ArrayList<>();

        bizTransactionList = new ArrayList<>();
        sourceList = new HashMap<>();
        destinationList = new HashMap<>();
    }

    public List<String> getInputEPCList() {
        return inputEPCList;
    }

    public void setInputEPCList(List<String> inputEPCList) {
        this.inputEPCList = inputEPCList;
    }

    public List<Integer> getInputQuantityList() {
        return inputQuantityList;
    }

    public void setInputQuantityList(List<Integer> inputQuantityList) {
        this.inputQuantityList = inputQuantityList;
    }

    public List<String> getOutputEPCList() {
        return outputEPCList;
    }

    public void setOutputEPCList(List<String> outputEPCList) {
        this.outputEPCList = outputEPCList;
    }

    public List<Integer> getOutputQuantityList() {
        return outputQuantityList;
    }

    public void setOutputQuantityList(List<Integer> outputQuantityList) {
        this.outputQuantityList = outputQuantityList;
    }

    public String getTransformationID() {
        return transformationID;
    }

    public void setTransformationID(String transformationID) {
        this.transformationID = transformationID;
    }

    public BizStepType getBizStep() {
        return bizStep;
    }

    public void setBizStep(BizStepType bizStep) {
        this.bizStep = bizStep;
    }

    public String getDisposition() {
        return disposition;
    }

    public void setDisposition(String disposition) {
        this.disposition = disposition;
    }

    public String getReadPoint() {
        return readPoint;
    }

    public void setReadPoint(String readPoint) {
        this.readPoint = readPoint;
    }

    public String getBizLocation() {
        return bizLocation;
    }

    public void setBizLocation(String bizLocation) {
        this.bizLocation = bizLocation;
    }

    public List<HashMap<String,String>> getBizTransactionList(){
        return bizTransactionList;
    }

    public void setBizTransactionList(List<HashMap<String,String>> bizTransactionList){
        this.bizTransactionList = bizTransactionList;
    }

    public Map<String, List<String>> getSourceList() {
        return sourceList;
    }

    public void setSourceList(Map<String, List<String>> sourceList) {
        this.sourceList = sourceList;
    }

    public Map<String, List<String>> getDestinationList() {
        return destinationList;
    }

    public void setDestinationList(Map<String, List<String>> destinationList) {
        this.destinationList = destinationList;
    }

    public JSONObject jsonObject() throws JSONException {
        CaptureUtil util = new CaptureUtil();

        JSONObject transformationEvent = super.jsonObject();
        transformationEvent = util.putEventType(transformationEvent, "TransformationEvent");

        // Optional Fields
        if (this.inputEPCList != null && this.inputEPCList.size() != 0) {
            transformationEvent = util.putInputEPCList(transformationEvent, inputEPCList);
        }

        if (this.inputQuantityList != null && !this.inputQuantityList.isEmpty()) {
            transformationEvent = util.putInputQuantityList(transformationEvent, inputQuantityList);
        }
        if (this.outputEPCList != null && this.outputEPCList.size() != 0) {
            transformationEvent = util.putOutputEPCList(transformationEvent, outputEPCList);
        }
        if (this.outputQuantityList != null && !this.outputQuantityList.isEmpty()) {
            transformationEvent = util.putOutputQuantityList(transformationEvent, outputQuantityList);
        }

        if (this.transformationID != null) {
            transformationEvent = util.putTransformationID(transformationEvent, transformationID);
        }
        if (this.bizStep != null) {
            transformationEvent = util.putBizStep(transformationEvent, bizStep);
        }
        if (this.disposition != null) {
            transformationEvent = util.putDisposition(transformationEvent, disposition);
        }
        if (this.readPoint != null) {
            transformationEvent = util.putReadPoint(transformationEvent, readPoint);
        }
        if (this.bizLocation != null) {
            transformationEvent = util.putBizLocation(transformationEvent, bizLocation);
        }
        if (this.bizTransactionList != null && !this.bizTransactionList.isEmpty()) {
            transformationEvent = util.putBizTransactionList(transformationEvent, bizTransactionList);
        }


        return transformationEvent;
    }
}