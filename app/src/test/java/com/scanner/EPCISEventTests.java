package com.scanner;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import com.scanner.cbv.BizStepType;
import com.scanner.event.EPCISDocument;
import com.scanner.event.ObjectEvent;

import java.util.ArrayList;
import java.util.List;

public class EPCISEventTests {

    @Test
    public void createEPCISdocument() throws JSONException {

        List<String> epcList = new ArrayList<>();
        epcList.add("urn:epc:id:sgtin:0614141.107346.2018");
        epcList.add("urn:epc:id:sgtin:0614141.107346.2019");
        epcList.add("urn:epc:id:sgtin:0614141.107346.2020");

        //Create EPCIS document
        EPCISDocument epcisDocument = new EPCISDocument();

        //Set fields
        epcisDocument.setSchemaVersion("1.2");
        epcisDocument.setFormat("application/ld+json");

        //Convert object to JSON
        JSONObject jsonObject = epcisDocument.jsonObject();

        //Display epcisDocument.
        System.out.println(jsonObject.toString(3));

        //Create EPCIS event
        ObjectEvent objEvent = new ObjectEvent();
        objEvent.setAction("ADD");
        objEvent.setEpcList(epcList);
        BizStepType bizStep = new BizStepType("packing");
        objEvent.setBizStep(bizStep);

        //Add event to Document
        epcisDocument.setEpcisBody(objEvent);

        //Convert object to JSON
        jsonObject = epcisDocument.jsonObject();

        //Display epcisDocument.
        System.out.println(jsonObject.toString(3));

        //Generate 2nd event
        ObjectEvent objEvent2 = new ObjectEvent();
        objEvent2.setAction("DELETE");
        objEvent2.setEpcList(epcList);
        bizStep = new BizStepType("destroying");
        objEvent2.setBizStep(bizStep);

        //Add event to Document
        epcisDocument.setEpcisBody(objEvent2);

        //Convert object to JSON
        jsonObject = epcisDocument.jsonObject();

        //Display epcisDocument.
        System.out.println(jsonObject.toString(3));
    }
}